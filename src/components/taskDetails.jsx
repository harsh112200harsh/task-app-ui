import React from "react";
import PropTypes from "prop-types";
import { format } from "date-fns";

const TaskDetail = ({ task, onEdit }) => {
    if (!task) {
        return (
            <div className="text-center text-gray-500">No task selected</div>
        );
    }

    return (
        <div className="container mx-auto p-4 border rounded-md shadow-md bg-white">
            <h2 className="text-2xl font-bold mb-4">Task Detail</h2>
            <div className="mb-4">
                <strong>Title:</strong>
                <p>{task.title}</p>
            </div>
            <div className="mb-4">
                <strong>Description:</strong>
                <p>{task.description}</p>
            </div>
            <div className="mb-4">
                <strong>Due Date:</strong>
                <p>{format(new Date(task.dueDate), "yyyy/MM/dd")}</p>
            </div>
            <div className="mb-4">
                <strong>Status:</strong>
                <p>{task.completed ? "Completed" : "Pending"}</p>
            </div>
            <button
                onClick={() => onEdit(task)}
                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-700"
            >
                Edit
            </button>
        </div>
    );
};

TaskDetail.propTypes = {
    task: PropTypes.shape({
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        dueDate: PropTypes.string.isRequired,
        completed: PropTypes.bool.isRequired,
    }),
    onEdit: PropTypes.func.isRequired,
};

export default TaskDetail;
