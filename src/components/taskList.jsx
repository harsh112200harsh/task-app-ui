import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getTasks, removeTask, editTask } from "../store/actions";
import { toast } from "react-toastify";
import { FiEdit, FiTrash2 } from "react-icons/fi";
import AddTask from "./addTask";
import Model from "./model";
import TaskDetail from "./taskDetails";
import EditTaskForm from "./editTask";

const TaskList = () => {
    const dispatch = useDispatch();
    const tasks = useSelector((state) => state.tasks);
    const [addTask, setAddTask] = useState(false);
    const [selectedTask, setSelectedTask] = useState(null);
    const [isEditing, setIsEditing] = useState(false);

    useEffect(() => {
        dispatch(getTasks());
    }, [dispatch]);

    const handleDelete = (id) => {
        dispatch(removeTask(id));
        toast.success("Task deleted successfully!");
    };

    const handleSave = (updatedTask) => {
        dispatch(editTask(updatedTask._id, updatedTask));
        setIsEditing(false);
        setSelectedTask(null);
    };

    const handleEdit = (task) => {
        setSelectedTask(task);
        setIsEditing(true);
    };

    return (
        <>
            {addTask && (
                <Model bgClick={() => setAddTask(false)}>
                    <AddTask />
                </Model>
            )}
            {selectedTask && (
                <Model
                    bgClick={() => {
                        setSelectedTask(null);
                        setIsEditing(false);
                    }}
                >
                    {isEditing ? (
                        <EditTaskForm
                            task={selectedTask}
                            onCancel={() => setIsEditing(false)}
                            onSave={handleSave}
                        />
                    ) : (
                        <TaskDetail task={selectedTask} onEdit={handleEdit} />
                    )}
                </Model>
            )}
            <div className="container mx-auto p-4">
                <div className="flex justify-between mb-10">
                    <h2 className="text-2xl font-bold mb-4">Task List</h2>
                    <button
                        onClick={() => {
                            setAddTask(true);
                        }}
                        className="bg-blue-500 text-white px-3 py-1 rounded-md hover:bg-blue-700"
                    >
                        Add Task
                    </button>
                </div>
                <div className="overflow-x-auto">
                    <table className="min-w-full bg-white border border-gray-300 w-full">
                        <thead>
                            <tr>
                                <th className="px-6 py-3 border-b-2 border-gray-300 text-left leading-4 text-blue-500 tracking-wider">
                                    Title
                                </th>
                            </tr>
                        </thead>
                        <tbody className="w-full">
                            {tasks.map((task) => (
                                <tr key={task._id} className="w-full">
                                    <td className="px-6 py-4 border-b border-gray-300">
                                        <div className="flex justify-between">
                                            <h2
                                                className="hover:text-blue-500 cursor-pointer"
                                                onClick={() =>
                                                    setSelectedTask(task)
                                                }
                                            >
                                                {task.title}
                                            </h2>
                                            <button
                                                onClick={() =>
                                                    handleDelete(task._id)
                                                }
                                                className="hidden md:inline-block bg-red-500 text-white px-3 py-1 rounded-md hover:bg-red-700"
                                            >
                                                Delete
                                            </button>
                                            <button
                                                onClick={(e) =>
                                                    handleDelete(task._id)
                                                }
                                                className="md:hidden text-red-500 hover:text-red-700"
                                            >
                                                <FiTrash2 size={20} />
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
};

export default TaskList;
