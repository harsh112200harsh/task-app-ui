import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTask } from "../store/actions";
import { toast } from "react-toastify";
import * as yup from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { subDays } from "date-fns";

const taskSchema = yup.object().shape({
    title: yup.string().required("Title is required"),
    description: yup.string().required("Description is required"),
    dueDate: yup
        .date()
        .min(subDays(new Date(), 0), "Due date must be in the future")
        .required("Due date is required"),
});

const AddTask = () => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [dueDate, setDueDate] = useState(new Date());
    const dispatch = useDispatch();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const newTask = {
            title,
            description,
            completed: false,
            dueDate,
        };

        try {
            await taskSchema.validate(newTask);
            dispatch(addTask(newTask));
            toast.success("Task added successfully!");
            setTitle("");
            setDescription("");
            setDueDate(new Date());
        } catch (error) {
            toast.error(error.message);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="container mx-auto p-4">
            <h2 className="text-2xl font-bold mb-4">Add Task</h2>
            <div className="mb-4">
                <input
                    type="text"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    placeholder="Title"
                    required
                    className="w-full p-2 border border-gray-300 rounded-md"
                />
            </div>
            <div className="mb-4">
                <textarea
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    placeholder="Description"
                    required
                    className="w-full p-2 border border-gray-300 rounded-md"
                />
            </div>
            <div className="mb-4">
                <DatePicker
                    selected={dueDate}
                    onChange={(date) => setDueDate(date)}
                    dateFormat="yyyy/MM/dd"
                    className="w-full p-2 border border-gray-300 rounded-md"
                    minDate={new Date()}
                    required
                />
            </div>
            <button
                type="submit"
                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-700"
            >
                Add Task
            </button>
        </form>
    );
};

export default AddTask;
