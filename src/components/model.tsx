import React from "react";

const Model = ({ children, bgClick }) => {
    return (
        <div
            onClick={bgClick}
            className="fixed inset-0 bg-black/30 z-30 w-full flex justify-center items-center p-10"
        >
            <div
                className="w-full max-w-2xl bg-white"
                onClick={(e) => e.stopPropagation()}
            >
                {children}
            </div>
        </div>
    );
};

export default Model;
