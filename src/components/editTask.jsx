import React, { useState } from "react";
import PropTypes from "prop-types";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const EditTaskForm = ({ task, onSave, onCancel }) => {
    const [title, setTitle] = useState(task.title);
    const [description, setDescription] = useState(task.description);
    const [dueDate, setDueDate] = useState(new Date(task.dueDate));
    const [completed, setCompleted] = useState(task.completed);

    const handleSubmit = (e) => {
        e.preventDefault();
        const updatedTask = {
            ...task,
            title,
            description,
            dueDate,
            completed,
        };
        onSave(updatedTask);
    };

    return (
        <form
            onSubmit={handleSubmit}
            className="container mx-auto p-4 border rounded-md shadow-md bg-white"
        >
            <h2 className="text-2xl font-bold mb-4">Edit Task</h2>
            <div className="mb-4">
                <input
                    type="text"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    placeholder="Title"
                    required
                    className="w-full p-2 border border-gray-300 rounded-md"
                />
            </div>
            <div className="mb-4">
                <textarea
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    placeholder="Description"
                    required
                    className="w-full p-2 border border-gray-300 rounded-md"
                />
            </div>
            <div className="mb-4">
                <DatePicker
                    selected={dueDate}
                    onChange={(date) => setDueDate(date)}
                    dateFormat="yyyy/MM/dd"
                    className="w-full p-2 border border-gray-300 rounded-md"
                    minDate={new Date()}
                    required
                />
            </div>
            <div className="mb-4">
                <label className="flex items-center space-x-3">
                    <input
                        type="checkbox"
                        checked={completed}
                        onChange={() => setCompleted(!completed)}
                        className="form-checkbox h-5 w-5 text-blue-600"
                    />
                    <span>Completed</span>
                </label>
            </div>
            <div className="flex space-x-4">
                <button
                    type="submit"
                    className="bg-green-500 text-white px-4 py-2 rounded-md hover:bg-green-700"
                >
                    Save
                </button>
                <button
                    type="button"
                    onClick={onCancel}
                    className="bg-gray-500 text-white px-4 py-2 rounded-md hover:bg-gray-700"
                >
                    Cancel
                </button>
            </div>
        </form>
    );
};

EditTaskForm.propTypes = {
    task: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        dueDate: PropTypes.string.isRequired,
        completed: PropTypes.bool.isRequired,
    }).isRequired,
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

export default EditTaskForm;
