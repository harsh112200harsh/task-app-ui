import axios from "axios";

const API_URL = "https://task-app-backend-api-p0rs.onrender.com/api/tasks"; // Adjust the API URL as needed

export const fetchTasks = async () => {
    const response = await axios.get(API_URL);
    return response.data;
};

export const createTask = async (task) => {
    const response = await axios.post(API_URL, task);
    return response.data;
};

export const updateTask = async (id, task) => {
    const response = await axios.put(`${API_URL}/${id}`, task);
    return response.data;
};

export const deleteTask = async (id) => {
    const response = await axios.delete(`${API_URL}/${id}`);
    return response.data;
};

export const fetchTaskById = async (id) => {
    const response = await axios.get(`${API_URL}/${id}`);
    return response.data;
};
