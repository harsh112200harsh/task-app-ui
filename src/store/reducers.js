import { combineReducers } from "redux";
import {
    GET_TASKS,
    CREATE_TASK,
    UPDATE_TASK,
    DELETE_TASK,
    GET_TASK_BY_ID,
} from "./actions";

const tasksReducer = (state = [], action) => {
    switch (action.type) {
        case GET_TASKS:
            return action.payload;
        case CREATE_TASK:
            return [...state, action.payload];
        case UPDATE_TASK:
            return state.map((task) =>
                task._id === action.payload._id ? action.payload : task
            );
        case DELETE_TASK:
            return state.filter((task) => task._id !== action.payload);
        default:
            return state;
    }
};

const taskReducer = (state = {}, action) => {
    switch (action.type) {
        case GET_TASK_BY_ID:
            return action.payload;
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    tasks: tasksReducer,
    task: taskReducer,
});

export default rootReducer;
