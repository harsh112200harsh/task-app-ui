import {
    fetchTasks,
    createTask,
    updateTask,
    deleteTask,
    fetchTaskById,
} from "../services/task";

// Action types
export const GET_TASKS = "GET_TASKS";
export const CREATE_TASK = "CREATE_TASK";
export const UPDATE_TASK = "UPDATE_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const GET_TASK_BY_ID = "GET_TASK_BY_ID";

// Action creators
export const getTasks = () => async (dispatch) => {
    const tasks = await fetchTasks();
    dispatch({
        type: GET_TASKS,
        payload: tasks,
    });
};

export const addTask = (task) => async (dispatch) => {
    const newTask = await createTask(task);
    dispatch({
        type: CREATE_TASK,
        payload: newTask,
    });
};

export const editTask = (id, task) => async (dispatch) => {
    const updatedTask = await updateTask(id, task);
    dispatch({
        type: UPDATE_TASK,
        payload: updatedTask,
    });
};

export const removeTask = (id) => async (dispatch) => {
    await deleteTask(id);
    dispatch({
        type: DELETE_TASK,
        payload: id,
    });
};

export const getTaskById = (id) => async (dispatch) => {
    const task = await fetchTaskById(id);
    dispatch({
        type: GET_TASK_BY_ID,
        payload: task,
    });
};
