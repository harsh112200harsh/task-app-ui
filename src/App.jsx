import { ToastContainer } from "react-toastify";
import AddTask from "./components/addTask";
import Model from "./components/model";
import TaskList from "./components/taskList";
import "react-toastify/dist/ReactToastify.css";
function App() {
    return (
        <>
            <ToastContainer />
            <div className="max-w-5xl mx-auto">
                <TaskList />
            </div>
        </>
    );
}

export default App;
